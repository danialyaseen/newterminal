<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<link rel="shortcut icon" href="https://crm.projectheadways.com/fav.png">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/style.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/font-awesome.min.css">
<script src="<?=base_url()?>assets/front/js/jquery.tools-1.2.5.min.js"></script>
<script src="<?=base_url()?>assets/front/js/jquery-ui-1.8.11.custom.min.js" language="javascript" type="text/javascript"></script>
<script src="<?=base_url()?>assets/front/js/jquery.colorbox-min.js"></script>
<script src="<?=base_url()?>assets/front/js/ccvalidations.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Payment Terminal</title>
<script>
    $(document).ready(function(){
        $(".ccinfo").show();
        $("a[rel='hint']").colorbox();
        $(":radio[name=cctype]").click(function(){
            if($(this).hasClass("isPayPal")){
                 $(".ccinfo").slideUp("fast");
            } else {
                 $(".ccinfo").slideDown("fast");
            }
            resetCCHightlight();
        });

        $("input[name=ccn]").bind('paste', function(e) {
                var el = $(this);
                setTimeout(function() {
                    var text = $(el).val();
                    resetCCHightlight();
                    checkNumHighlight(text);
                }, 100);
        });
    });
</script>
      

<noscript>
<style>
	.noscriptCase { display:none; }
	#accordion .pane { display:block;}
</style>
</noscript>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
   <script type="text/javascript">
   Stripe.setPublishableKey('<?=$brands[0]["BrandPUBKEY"]?>');

   function stripeResponseHandler(status, response) {
   if (response.error) {
       console.log(response);
   $('.submit-btn input').css({"cursor":"pointer","opacity":"1"});
   // show the errors on the form, hide the submit button while processing
   $('form.pppt_form').prepend('<div id="stripe_error" style="display:none" class="ui-widget"><div class="ui-state-error ui-corner-all" style="padding: 0 .7em;"><div id="stripe_error_message" ></div></div></div><br />');
   $('#stripe_error').css("display","block");
  $("#stripe_error_message").html(response.error.message);
  $('#loading-sp').hide();

  jQuery.ajax({
           url: window.location.origin + '/save_attempt',
           type: "POST",
           data: {id: 'c86d5733-65da-11ed-945d-52540009454a'},
           async: false,  // Has to be false to be able to return response
           //dataType: "",  // Has to be false to be able to return response
           success: function(response) {
               console.log(response);
               if(response=='0'){
                   alert('Your link has expired. Please contact Sale for further Inquiry.');
                   location.reload();
               }
               else{}
           },
           beforeSend: function()
           {

           }
       });  // JQUERY Native Ajax End

   return false;
   } else {
   $('.submit-btn input').css({"cursor":"none","opacity":"0.3"});	
   $('#stripe_error').css("display","none");	
   var form$ = $("#ff1");
   // token contains id, last4, and card type
   var token = response['id'];
   // insert the token into the form so it gets submitted to the server
   form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
   // and submit
           form$.get(0).submit();
       }
   }

   $(document).ready(function() {
   $("#ff1").submit(function(event) {

    if($("input[type='radio'][name='cctype']:checked").val()=="PP"){
      document.ff1.submit();
    } else { 

        var validation = checkForm();

        if(validation){
          $('#loading-sp').show();
       Stripe.createToken({
       /* User Details */
       name: $('#ccname').val(),
       address_line1: $('#address').val(),
       address_zip: $('#zip').val(),
       address_state: $('#state').val(),
       address_country: $('#country').val(),
       address_city: $('#city').val(),	
       /* Card Details */	
       number: $('#ccn').val(),
       cvc: $('#cvv').val(),
       exp_month: $('#exp1').val(),
       exp_year: $('#exp2').val().substr(2)
       }, stripeResponseHandler);
       return false;

     }
    }
    });

    

   });
   </script>
</head>
<body><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" style="padding-bottom: 0px; padding-right: 0px; display: none;"><div id="cboxWrapper" style=""><div style=""><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxLoadedContent" class="" style="width: 0px; height: 0px; overflow: hidden;"></div><div id="cboxLoadingOverlay" class="" style=""></div><div id="cboxLoadingGraphic" class="" style=""></div><div id="cboxTitle" class="" style=""></div><div id="cboxCurrent" class="" style=""></div><div id="cboxNext" class="" style=""></div><div id="cboxPrevious" class="" style=""></div><div id="cboxSlideshow" class="" style=""></div><div id="cboxClose" class="" style=""></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div></div><!--
/******************************************************************************
#                      PHP Stripe Payment Terminal v1.3
#******************************************************************************
#      Author:     Convergine.com
#      Email:      info@convergine.com
#      Website:    http://www.convergine.com
#
#
#      Version:    1.3
#      Copyright:  (c) 2013 - Convergine.com
#      
#*******************************************************************************/
-->
<script type="text/JavaScript">
<!--

function checkForm() {
	var err=0;

    for (var i=0; i < document.ff1.cctype.length; i++){
       if (document.ff1.cctype[i].checked){
          var cctype = document.ff1.cctype[i].value;
        }
    }


if (document.getElementById('fname').value==0) {
    if (err==0) {
        document.getElementById('fname').focus();
    }
    document.getElementById('fname').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('lname').value==0) {
    if (err==0) {
        document.getElementById('lname').focus();
    }
    document.getElementById('lname').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('address').value==0) {
    if (err==0) {
        document.getElementById('address').focus();
    }
    document.getElementById('address').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('city').value==0) {
    if (err==0) {
        document.getElementById('city').focus();
    }
    document.getElementById('city').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('state').value==0) {
    if (err==0) {
        document.getElementById('state').focus();
    }
    document.getElementById('state').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('zip').value==0) {
    if (err==0) {
        document.getElementById('zip').focus();
    }
    document.getElementById('zip').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('email').value==0) {
    if (err==0) {
        document.getElementById('email').focus();
    }
    document.getElementById('email').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('bank_name').value==0) {
    if (err==0) {
        document.getElementById('bank_name').focus();
    }
    document.getElementById('bank_name').style.backgroundColor='#ffa5a5';
    err=1;
}
if (document.getElementById('phone').value==0) {
    if (err==0) {
        document.getElementById('phone').focus();
    }
    document.getElementById('phone').style.backgroundColor='#ffa5a5';
    err=1;
}

if(cctype!="PP"){
                if (document.getElementById('ccn').value==0) {
                if (err==0) {
                    document.getElementById('ccn').focus();
                }
                document.getElementById('ccn').style.backgroundColor='#ffa5a5';
                err=1;
            }
                if (document.getElementById('ccname').value==0) {
                if (err==0) {
                    document.getElementById('ccname').focus();
                }
                document.getElementById('ccname').style.backgroundColor='#ffa5a5';
                err=1;
            }
                if (document.getElementById('exp1').value==0) {
                if (err==0) {
                    document.getElementById('exp1').focus();
                }
                document.getElementById('exp1').style.backgroundColor='#ffa5a5';
                err=1;
            }
                if (document.getElementById('exp2').value==0) {
                if (err==0) {
                    document.getElementById('exp2').focus();
                }
                document.getElementById('exp2').style.backgroundColor='#ffa5a5';
                err=1;
            }
                if (document.getElementById('cvv').value==0) {
                if (err==0) {
                    document.getElementById('cvv').focus();
                }
                document.getElementById('cvv').style.backgroundColor='#ffa5a5';
                err=1;
            }
        if(err==0){

        //check credit card.
        var ccn = document.getElementById("ccn").value;
        if(!isValidCardNumber(ccn)){
            alert("Invalid credit card number. Please check your input and try again");
            return false;
        }
        if(isExpiryDate(document.getElementById("exp2").value,document.getElementById("exp1").value)==false){
            alert("Credit Card expiry date is in the past! Please adjust your input.");
            return false;
        }

        if(!isCardTypeCorrect(ccn,cctype)){
            alert("Invalid credit card number/type combination. Please check your input and try again");
            return false;
        }
    }
}





	var reg1 = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/; // not valid
	var reg2 = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,5}|[0-9]{1,3})(\]?)$/; // valid
	if (document.getElementById('email').value==0 || !reg2.test(document.getElementById('email').value)) {
	if (err==0) {
		document.getElementById('email').focus();
	}
	document.getElementById('email').style.backgroundColor='#ffa5a5';
	err=1;
	}

if (err==0) {
		return true;
	} else {
		alert("Please complete all highlighted fields to continue.");
		return false;
	}
}

function checkFieldBack(fieldObj) {
	if (fieldObj.value!=0) {
		fieldObj.style.backgroundColor='#F8F8F8';
	}
}
function noAlpha(obj){
	reg = /[^0-9.,]/g;
	obj.value =  obj.value.replace(reg,"");
 }


//-->
</script><div class="bg" style="width:100%;display:none; height:200%; background-color:gray; z-index: 999; position: absolute; opacity: 0.56;">
    <h1>Processing take a minute</h1>
</div>
<style>
    body{
        background:#fff;
    }
    .payment{
        width: 70%;
        margin: 20px auto;
        background-image:url('https://bluetickpro.co.uk/assets/images/box-bg.png');
        background-repeat:no-repeat;
        background-size:cover;
        
    }
.totalAmount p.totalPrice {
    font-size: 35px;
    font-weight: 700;
    color: #212529;
}
.totalAmount p.totalLabel {
    font-size: 18px;
    font-weight: 600;
    text-transform: uppercase;
    margin: 0;
    color: #7a7a7a;
}
.payment h3 {
    margin-bottom: 40px;
    margin-top: -28px;
    width: 100%;
    padding: 10px 0;
    margin-left: 0;
    font-weight: 700;
    font-family: 'Open Sans', sans-serif;
    color: #000;
    background: #ffffff;
    margin-top: 0;
    margin-bottom: 10px;
    font-size: 18px;
    text-transform: uppercase;
}
.digi-form{
    background: #fff;
    padding: 20px;
    border-radius: 20px;
        margin-bottom: 20px;
}
</style>

<section class="payment container">
    <div class="">
        <div class="row">
        <div class="col-md-12" align="center">
            <br>
            <img src="<?=base_url()?><?=$brands[0]["BrandLogo"]?>" style="width: 150px;">
        </div>
        
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 payment-way" style="border-radius: 12px;box-shadow: none;border: 0;margin: 20px 0;     background: transparent;">
            <!--<div class="digi-form ">-->
            <!--    <div class="checkbox-form">-->
            <!--        <h3 class="">Payment Information</h3>-->
            <!--        <div class="row">-->
            <!--            <div class="col-md-8">-->
            <!--                <p>Description:</p>-->
            <!--                <p>test</p>-->
            <!--            </div>-->
                    
            <!--            <div class="col-md-4">-->
            <!--                <div class="totalAmount">-->
            <!--                    <p class="totalLabel">Total Amount</p>-->
            <!--                    <p class="totalPrice">USD10.00</p>-->
                                
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->
            <div class="digi-form ">
                <div class="checkbox-form">
                    <div class="row">
                        
                        <form id="ff1" name="ff1" method="post" action="<?php echo base_url('handleStripePayment');?>" enctype="multipart/form-data" onsubmit="return false;" class="pppt_form">
                                                            <!-- PAYMENT BLOCK -->
                                <div class="col-md-12">
                                    <!--<h3 class="">Payment Information</h3>-->
                                </div>
                                                                    <div class="col-md-6">
                                        <div class="checkout-form-list">
                                            <!--<label>Description:</label>-->
                                            <input name="item_description" id="item_description" type="hidden" class="long-field" value="<?=$customerdata[0]["description"]?>" onkeyup="checkFieldBack(this);" readonly="">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="checkout-form-list">
                                            <!--<label>Amount: USD</label>-->
                                            <?php 
                                            $amount = $customerdata[0]["amount"];
                                            $tax = $customerdata[0]["tax"];
                                            $discount = $customerdata[0]["discount"];
                                            $discount_type = $customerdata[0]["discount_type"];
                                            $total_amount = $amount;


                                            if($discount_type=='InAmount'){
                                                if($discount==0){

                                                }
                                                else{
                                                    $discount = $customerdata[0]["discount"];
                                                }
                                            }
                                            else if($discount_type=='Percent'){
                                                if($discount==0){

                                                }
                                                else{
                                                    $discount = $amount*$discount/100;
                                                }
                                            }
                                            else{
                                                $discount=0;
                                            }
                                            $total_amount = $total_amount-$discount;


                                            if($tax==0 || $tax==''){
                                                //$total_amount = $customerdata[0]["amount"];
                                            }
                                            else{
                                                $tax = $total_amount*$customerdata[0]["tax"]/100;
                                                $total_amount = $total_amount+$tax;
                                            }
                                            ?>
                                            <input name="amount" id="amount" type="hidden" class="small-field" value="<?=$total_amount?>" onkeyup="checkFieldBack(this);noAlpha(this);" onkeypress="noAlpha(this);" readonly="">
                                        </div>
                                    </div>

                                    <input name="linkID" type="hidden" value="<?=$customerdata[0]["LinkID"]?>">
                                    <input name="tw" type="hidden" value="0">
                                    <input name="pt" type="hidden" value="3">
                                    <input name="gateway" type="hidden" value="2">

                                                                <!-- PAYMENT BLOCK -->

                                <!--<div class="col-md-12">-->
                                <!--    <hr>-->
                                <!--</div>-->


                                <!-- BILLING BLOCK -->
                                <div class="col-md-12">
                                    <h3>Billing Details</h3>
                                </div>

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>First Name:</label>
                                        <input name="fname" id="fname" type="text" class="long-field" value="<?=$customerdata[0]["first_name"]?>" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Last Name:</label>
                                        <input name="lname" id="lname" type="text" class="long-field" value="<?=$customerdata[0]["last_name"]?>" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                <div style="display: none;" class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Address:</label>
                                        <input value="15205 North Kierland Blvd" name="address" id="address" type="text" class="long-field" value="" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Country:</label>
                                        <select name="country" id="country" class="long-field" onchange="checkFieldBack(this);">
                                            <option value="">Please Select</option>
                                            <option value="US" selected="">United States</option>
                                            <option value="CA">Canada</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="AU">Australia</option>
                                            <option value="AF">Afghanistan</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda
                                            </option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BZ">Belize</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegovina
                                            </option>
                                            <option value="BW">Botswana</option>
                                            <option value="BR">Brazil</option>
                                            <option value="BN">Brunei Darussalam
                                            </option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="BI">Burundi</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CF">Central African
                                                Republic
                                            </option>
                                            <option value="TD">Chad</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CX">Christmas Island
                                            </option>
                                            <option value="CC">Cocos (Keeling)
                                                Islands
                                            </option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CD">Congo, The Democratic
                                                Republic of the
                                            </option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="CI">Cote D`Ivoire</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic
                                            </option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea
                                            </option>
                                            <option value="ER">Eritrea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="ET">Ethiopia</option>
                                            <option value="FK">Falkland Islands
                                                (Malvinas)
                                            </option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia
                                            </option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GN">Guinea</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="HT">Haiti</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IR">Iran (Islamic Republic
                                                Of)
                                            </option>
                                            <option value="IQ">Iraq</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KP">Korea North</option>
                                            <option value="KR">Korea South</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Laos</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LR">Liberia</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands
                                            </option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia</option>
                                            <option value="MD">Moldova</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="AN">Netherlands Antilles
                                            </option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PS">Palestine Autonomous
                                            </option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea
                                            </option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="PR">Puerto Rico</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russian Federation
                                            </option>
                                            <option value="RW">Rwanda</option>
                                            <option value="VC">Saint Vincent and the
                                                Grenadines
                                            </option>
                                            <option value="MP">Saipan</option>
                                            <option value="SM">San Marino</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SK">Slovak Republic</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SO">Somalia</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="KN">St. Kitts/Nevis</option>
                                            <option value="LC">St. Lucia</option>
                                            <option value="SD">Sudan</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="SY">Syria</option>
                                            <option value="TW">Taiwan</option>
                                            <option value="TI">Tajikistan</option>
                                            <option value="TZ">Tanzania</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago
                                            </option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos
                                                Islands
                                            </option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UG">Uganda</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates
                                            </option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Viet Nam</option>
                                            <option value="VG">Virgin Islands
                                                (British)
                                            </option>
                                            <option value="VI">Virgin Islands (U.S.)
                                            </option>
                                            <option value="WF">Wallis and Futuna
                                                Islands
                                            </option>
                                            <option value="YE">Yemen</option>
                                            <option value="YU">Yugoslavia</option>
                                            <option value="ZM">Zambia</option>
                                            <option value="ZW">Zimbabwe</option>
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>State/Province:</label>
                                        <select name="state" id="state" class="long-field" onchange="checkFieldBack(this);">
                                            <option value="">Please Select</option>
                                            <optgroup label="Australian Provinces">
                                                <option value="-AU-NSW">New South
                                                    Wales
                                                </option>
                                                <option value="-AU-QLD">
                                                    Queensland
                                                </option>
                                                <option value="-AU-SA">South
                                                    Australia
                                                </option>
                                                <option value="-AU-TAS">Tasmania
                                                </option>
                                                <option value="-AU-VIC">Victoria
                                                </option>
                                                <option value="-AU-WA">Western
                                                    Australia
                                                </option>
                                                <option value="-AU-ACT">Australian
                                                    Capital Territory
                                                </option>
                                                <option value="-AU-NT">Northern
                                                    Territory
                                                </option>
                                            </optgroup>
                                            <optgroup label="Canadian Provinces">
                                                <option value="AB">Alberta</option>
                                                <option value="BC">British Columbia
                                                </option>
                                                <option value="MB">Manitoba</option>
                                                <option value="NB">New Brunswick
                                                </option>
                                                <option value="NF">Newfoundland</option>
                                                <option value="NT">Northwest
                                                    Territories
                                                </option>
                                                <option value="NS">Nova Scotia</option>
                                                <option value="NVT">Nunavut</option>
                                                <option value="ON">Ontario</option>
                                                <option value="PE">Prince Edward
                                                    Island
                                                </option>
                                                <option value="QC">Quebec</option>
                                                <option value="SK">Saskatchewan</option>
                                                <option value="YK">Yukon</option>
                                            </optgroup>
                                            <optgroup label="US States">
                                                <option value="AL">Alabama</option>
                                                <option value="AK">Alaska</option>
                                                <option value="AZ">Arizona</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="BVI">British Virgin
                                                    Islands
                                                </option>
                                                <option value="CA">California</option>
                                                <option value="CO">Colorado</option>
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="GU">Guam</option>
                                                <option value="HI">Hawaii</option>
                                                <option value="ID">Idaho</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IN">Indiana</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MP">Mariana Islands
                                                </option>
                                                <option value="MPI">Mariana Islands
                                                    (Pacific)
                                                </option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts
                                                </option>
                                                <option value="MI">Michigan</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NV">Nevada</option>
                                                <option value="NH">New Hampshire
                                                </option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina
                                                </option>
                                                <option value="ND">North Dakota</option>
                                                <option value="OH">Ohio</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="OR">Oregon</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="PR">Puerto Rico</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina
                                                </option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="TX">Texas</option>
                                                <option value="UT">Utah</option>
                                                <option value="VT">Vermont</option>
                                                <option value="USVI">VI U.S. Virgin
                                                    Islands
                                                </option>
                                                <option value="VA">Virginia</option>
                                                <option value="WA">Washington</option>
                                                <option value="DC">Washington, D.C.
                                                </option>
                                                <option value="WV">West Virginia
                                                </option>
                                                <option value="WI">Wisconsin</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                            <!-- FOR STRIPE UK -->
                                            <optgroup label="England">
                                                <option>Bedfordshire</option>
                                                <option>Berkshire</option>
                                                <option>Bristol</option>
                                                <option>Buckinghamshire
                                                </option>
                                                <option>Cambridgeshire
                                                </option>
                                                <option>Cheshire</option>
                                                <option>City of London
                                                </option>
                                                <option>Cornwall</option>
                                                <option>Cumbria</option>
                                                <option>Derbyshire</option>
                                                <option>Devon</option>
                                                <option>Dorset</option>
                                                <option>Durham</option>
                                                <option>East Riding
                                                    of Yorkshire
                                                </option>
                                                <option>East Sussex</option>
                                                <option>Essex</option>
                                                <option>Gloucestershire
                                                </option>
                                                <option>Greater London
                                                </option>
                                                <option>Greater
                                                    Manchester
                                                </option>
                                                <option>Hampshire</option>
                                                <option>Herefordshire</option>
                                                <option>Hertfordshire</option>
                                                <option>Isle of Wight</option>
                                                <option>Kent</option>
                                                <option>Lancashire</option>
                                                <option>Leicestershire
                                                </option>
                                                <option>Lincolnshire</option>
                                                <option>Merseyside</option>
                                                <option>Norfolk</option>
                                                <option>North Yorkshire
                                                </option>
                                                <option>Northamptonshire
                                                </option>
                                                <option>Northumberland
                                                </option>
                                                <option>Nottinghamshire
                                                </option>
                                                <option>Oxfordshire</option>
                                                <option>Rutland</option>
                                                <option>Shropshire</option>
                                                <option>Somerset</option>
                                                <option>South Yorkshire
                                                </option>
                                                <option>Staffordshire</option>
                                                <option>Suffolk</option>
                                                <option>Surrey</option>
                                                <option>Tyne and Wear</option>
                                                <option>Warwickshire</option>
                                                <option>West Midlands</option>
                                                <option>West Sussex</option>
                                                <option>West Yorkshire
                                                </option>
                                                <option>Wiltshire</option>
                                                <option>Worcestershire
                                                </option>
                                            </optgroup>
                                            <optgroup label="Scotland">
                                                <option>Aberdeenshire</option>
                                                <option>Angus</option>
                                                <option>Argyllshire</option>
                                                <option>Ayrshire</option>
                                                <option>Banffshire</option>
                                                <option>Berwickshire</option>
                                                <option>Buteshire</option>
                                                <option>Cromartyshire</option>
                                                <option>Caithness</option>
                                                <option>Clackmannanshire
                                                </option>
                                                <option>Dumfriesshire</option>
                                                <option>Dunbartonshire
                                                </option>
                                                <option>East Lothian</option>
                                                <option>Fife</option>
                                                <option>Inverness-shire
                                                </option>
                                                <option>Kincardineshire
                                                </option>
                                                <option>Kinross</option>
                                                <option>
                                                    Kirkcudbrightshire
                                                </option>
                                                <option>Lanarkshire</option>
                                                <option>Midlothian</option>
                                                <option>Morayshire</option>
                                                <option>Nairnshire</option>
                                                <option>Orkney</option>
                                                <option>Peeblesshire</option>
                                                <option>Perthshire</option>
                                                <option>Renfrewshire</option>
                                                <option>Ross-shire</option>
                                                <option>Roxburghshire</option>
                                                <option>Selkirkshire</option>
                                                <option>Shetland</option>
                                                <option>Stirlingshire</option>
                                                <option>Sutherland</option>
                                                <option>West Lothian</option>
                                                <option>Wigtownshire</option>
                                            </optgroup>
                                            <optgroup label="Wales">
                                                <option>Anglesey</option>
                                                <option>Brecknockshire
                                                </option>
                                                <option>Caernarfonshire
                                                </option>
                                                <option>Carmarthenshire
                                                </option>
                                                <option>Cardiganshire</option>
                                                <option>Denbighshire</option>
                                                <option>Flintshire</option>
                                                <option>Glamorgan</option>
                                                <option>Merioneth</option>
                                                <option>Monmouthshire</option>
                                                <option>Montgomeryshire
                                                </option>
                                                <option>Pembrokeshire</option>
                                                <option>Radnorshire</option>
                                            </optgroup>
                                            <optgroup label="Northern Ireland">
                                                <option>Antrim</option>
                                                <option>Armagh</option>
                                                <option>Down</option>
                                                <option>Fermanagh</option>
                                                <option>Londonderry</option>
                                                <option>Tyrone</option>
                                            </optgroup>
                                            <!-- FOR STRIPE UK END-->
                                            <option value="N/A">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>City:</label>
                                        <input name="city" id="city" type="text" class="long-field" value="" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>ZIP/Postal Code:</label>
                                        <input name="zip" id="zip" type="text" class="small-field" value="" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>E-mail:</label>
                                        <input name="email" id="email" type="text" class="long-field" value="<?=$customerdata[0]["email"]?>" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                <div style="display: none" class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Bank Name:</label>
                                        <input value="sada" name="bank_name" id="bank_name" type="text" class="long-field" value="" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Phone:</label>
                                        <input name="phone" id="phone" type="text" class="long-field" value="<?=$customerdata[0]["number"]?>" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <label>Card Type:</label>
                                    <div class="checkout-form-list">
                                        <div class="form-group radio-green">
                                            <input id="radio3" type="radio" name="card_type" value="Debit" checked="">
                                            <label for="radio3"> Debit </label>
                                            <input id="radio4" type="radio" name="card_type" value="Credit" class="m-top-10">
                                            <label for="radio4">Credit</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <!-- BILLING BLOCK -->

                                <div class="clearfix"></div>
                                <div class="col-md-12">
                                    <h3>Credit Card Information</h3>
                                </div>

                                <div class="col-md-12">
                                    <label>I have:</label>
                                    <div class="ship-card">
                                        <ul>
                                            <li><a href="#">
                                                    <div class="checkout-form-list">
                                                        <div class="form-group radio-green">
                                                            <input id="radio1" name="cctype" type="radio" value="V" class="lft-field">
                                                            <label for="radio1"><img src="<?=base_url()?>assets/images/card1.jpg"></label>
                                                        </div>
                                                    </div> </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <div class="checkout-form-list">
                                                        <div class="form-group radio-green">
                                                            <input id="radio2" name="cctype" type="radio" value="M" class="lft-field">
                                                            <label for="radio2"><img src="<?=base_url()?>assets/images/card2.jpg"></label>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                                                                        <li>
                                                <a href="#">
                                                    <div class="checkout-form-list">
                                                        <div class="form-group radio-green">
                                                            <input id="radio44" name="cctype" type="radio" value="D" class="lft-field">
                                                            <label for="radio44"><img src="<?=base_url()?>assets/images/card4.jpg"></label>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                                                                   
                                            
                                            <li>
                                                <a href="#">
                                                    <div class="checkout-form-list">
                                                        <div class="form-group radio-green">
                                                            <input id="radio33" name="cctype" type="radio" value="A" class="lft-field">
                                                            <label for="radio33"><img src="<?=base_url()?>assets/images/card3.jpg"></label>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li> 
                                            
                                        </ul>
                                    </div>
                                </div>

                                <!-- CREDIT CARD BLOCK -->
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Card Number: </label>
                                        <input id="ccn" type="text" class="long-field" onkeyup="checkNumHighlight(this.value);checkFieldBack(this);noAlpha(this);" value="" onkeypress="checkNumHighlight(this.value);noAlpha(this);" onblur="CHECKVALIDATE(this.value);" onchange="checkNumHighlight(this.value);" maxlength="16">
                                        <span class="ccresult"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Name on Card:</label>
                                        <input name="ccname" id="ccname" type="text" class="long-field" onkeyup="checkFieldBack(this);">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Expiration Month:</label>
                                        <select name="exp1" id="exp1" class="small-field" onchange="checkFieldBack(this);">
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                            <option value="04">04</option>
                                            <option value="05">05</option>
                                            <option value="06">06</option>
                                            <option value="07">07</option>
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                            <option value="12">12</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Expiration Year:</label>
                                        <select name="exp2" id="exp2" class="small-field" onchange="checkFieldBack(this);">
                                            <option value="2022">2022</option><option value="2023">2023</option><option value="2024">2024</option><option value="2025">2025</option><option value="2026">2026</option><option value="2027">2027</option><option value="2028">2028</option><option value="2029">2029</option><option value="2030">2030</option><option value="2031">2031</option>                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="checkout-form-list">
                                        <label>Card CVV:</label>
                                        <input id="cvv" type="text" maxlength="5" class="small-field" onkeyup="checkFieldBack(this);noAlpha(this);">
                                    </div>
                                </div>

                                <div class="col-md-2 row">


                                    <!--<button id="" data-toggle="modal" data-target="#myModal">?</button>-->

                                </div>



                                <div class="col-md-4">
                                    <div class="checkout-form-list">
                                        <label> Statement Descriptor </label>
                                        <p> <?=$brands[0]["BrandName"]?> </p>
                                    </div>
                                </div>

                                <input type="hidden" name="process" value="yes">

                                                                                <div class="col-md-12 privacy-terms">
                                                    <input onchange="this.setCustomValidity(validity.valueMissing ? 'Please indicate that you accept the Terms and Conditions' : '');"  type="checkbox" name="checkbox" id="tos" required>
                                                                                                        <label>I accept <a href="<?=$brands[0]["BrandTerms"]?>" target="_blank">Terms condition</a> and <a href="<?=$brands[0]["BrandPolicy"]?>" target="_blank">Privacy Policy</a>.</label>
                                                </div>
                                                                                

                                <div class="col-md-12">
                                    <input type="submit" name="" value="submit" class="btn">
                                </div>
                                <!-- CREDIT CARD BLOCK -->
                                                    </form>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 payment-way" style="border-radius: 12px;box-shadow: none;border: 0;margin: 20px 0;">-->
            
        <!--</div>-->
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 payment-way" style="border-radius: 12px;box-shadow: none;border: 0;margin: 20px 0;     background: transparent;">
            <div class="digi-form ">
                <div class="checkbox-form">
                    <h3 class="">Invoice</h3>
                    <p><?=$customerdata[0]["services"]?></p>
                    <div>
                        <?php
                        $currencysign = '';
                        if($customerdata[0]["currency"]=='USD'){
                            $currencysign = '$';
                        } 
                        else{
                            $currencysign = 'CAD';
                        }
                        ?>
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th>Items</th>
                                <!--<th>QTY</th>-->
                              </tr>
                            </thead>
                            <tbody>
                                                              <tr>
                                <td><?=$customerdata[0]["services"]?></td>
                                <td>$<?=$customerdata[0]["amount"]?></td>
                                <!--<td>1</td>-->
                              </tr>
                              
                              <tr>
                                <td>Discount</td>
                                <td><?=$currencysign.''.$discount?></td>
                                <!--<td>1</td>-->
                              </tr>
                              <tr>
                                <td>Tax(<?=$customerdata[0]["tax"]?>%)</td>
                                <td><?=$currencysign.''.$tax?></td>
                                <!--<td>1</td>-->
                              </tr>
                                                          </tbody>
                            <!--<thead>-->
                            <!--  <tr>-->
                            <!--    <th style="border-bottom: 0;">Total Amount</th>-->
                            <!--    <th style="border-bottom: 0;">USD10.00</th>-->
                            <!--  </tr>-->
                            <!--</thead>-->
                        </table>
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th style="">Total Amount</th>
                                <th style="float: right;"><?=$currencysign?><?=number_format($total_amount,2)?></th>
                              </tr>
                            </thead>
                        </table>
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th>Customer Details</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Customer Name</td>
                                    <td><?=$customerdata[0]["first_name"].' '.$customerdata[0]["last_name"]?></td>
                                </tr>
                                <tr>
                                    <td>Customer Email</td>
                                    <td><?=$customerdata[0]["email"]?></td>
                                </tr>
                                <tr>
                                    <td>Customer Number</td>
                                    <td><?=$customerdata[0]["number"]?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        
        </div>
    </div>
</section>

<div class="s-chart">
    <!-- The Modal -->
    <div class="modal fade" id="myModal" role="dialog">

        <!-- Modal content -->
        <div class="modal-content">

            <span class="close">×</span>


            <p>For <strong>Visa</strong>, <strong>MasterCard</strong>, and <strong>Discover</strong> cards, the card code is the last 3 digit number located on the BACK of your card on or above your signature line.</p>

            <p>For <strong>American Express</strong> card, it is the 4 digits on the FRONT above the end of your card number</p>

            <img src="./../assets/front/images/c1.jpg" class="img-responsive">


        </div>
    </div>
</div>


<div class="pppro_footer"></div>



<style type="text/css">
    #loading-sp {
        display: none;
        background: #000;
        opacity: 0.77;
        bottom: 0;
        left: 0;
        position: fixed;
        right: 0;
        top: 0;
        z-index: 9999999;
    }

    #load {
        position: absolute;
        height: 36px;
        top: 40%;

        overflow: visible;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        cursor: default;
        color: #fff;
        text-align: center;
        width: 100%;
    }

    #load div {
        position: absolute;
        width: 20px;
        height: 36px;
        opacity: 0;
        font-family: Helvetica, Arial, sans-serif;
        animation: move 2s linear infinite;
        -o-animation: move 2s linear infinite;
        -moz-animation: move 2s linear infinite;
        -webkit-animation: move 2s linear infinite;
        transform: rotate(180deg);
        -o-transform: rotate(180deg);
        -moz-transform: rotate(180deg);
        -webkit-transform: rotate(180deg);
        color: #cdcdcd;
        font-weight: bold;
        font-size: 16px;
    }

    #load div:nth-child(2) {
        animation-delay: 0.2s;
        -o-animation-delay: 0.2s;
        -moz-animation-delay: 0.2s;
        -webkit-animation-delay: 0.2s;
    }

    #load div:nth-child(3) {
        animation-delay: 0.4s;
        -o-animation-delay: 0.4s;
        -webkit-animation-delay: 0.4s;
        -webkit-animation-delay: 0.4s;
    }

    #load div:nth-child(4) {
        animation-delay: 0.6s;
        -o-animation-delay: 0.6s;
        -moz-animation-delay: 0.6s;
        -webkit-animation-delay: 0.6s;
    }

    #load div:nth-child(5) {
        animation-delay: 0.8s;
        -o-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        -webkit-animation-delay: 0.8s;
    }

    #load div:nth-child(6) {
        animation-delay: 1s;
        -o-animation-delay: 1s;
        -moz-animation-delay: 1s;
        -webkit-animation-delay: 1s;
    }

    #load div:nth-child(7) {
        animation-delay: 1.2s;
        -o-animation-delay: 1.2s;
        -moz-animation-delay: 1.2s;
        -webkit-animation-delay: 1.2s;
    }

    @keyframes move {
        0% {
            left: 0;
            opacity: 0;
        }
        35% {
            left: 41%;
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        65% {
            left: 59%;
            -moz-transform: rotate(0deg);
            -webkit-transform: rotate(0deg);
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        100% {
            left: 100%;
            -moz-transform: rotate(-180deg);
            -webkit-transform: rotate(-180deg);
            -o-transform: rotate(-180deg);
            transform: rotate(-180deg);
            opacity: 0;
        }
    }

    @-moz-keyframes move {
        0% {
            left: 0;
            opacity: 0;
        }
        35% {
            left: 41%;
            -moz-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        65% {
            left: 59%;
            -moz-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        100% {
            left: 100%;
            -moz-transform: rotate(-180deg);
            transform: rotate(-180deg);
            opacity: 0;
        }
    }

    @-webkit-keyframes move {
        0% {
            left: 0;
            opacity: 0;
        }
        35% {
            left: 41%;
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        65% {
            left: 59%;
            -webkit-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        100% {
            left: 100%;
            -webkit-transform: rotate(-180deg);
            transform: rotate(-180deg);
            opacity: 0;
        }
    }

    @-o-keyframes move {
        0% {
            left: 0;
            opacity: 0;
        }
        35% {
            left: 41%;
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        65% {
            left: 59%;
            -o-transform: rotate(0deg);
            transform: rotate(0deg);
            opacity: 1;
        }
        100% {
            left: 100%;
            -o-transform: rotate(-180deg);
            transform: rotate(-180deg);
            opacity: 0;
        }
    }
</style>
<div id="loading-sp" style="display: none;">
    <div id="load">
        Please Wait. Your transaction is processing...
    </div>
</div>

<script>

  document.getElementById("tos").setCustomValidity("Please indicate that you accept the Terms and Conditions");

</script>
</body></html>