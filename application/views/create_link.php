<html><head>
    <link rel="shortcut icon" href="https://crm.projectheadways.com/fav.png">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>New Payment Form</title>

    <!-- Css start -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/style.css">
    <!-- Css end -->

</head>
<body>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    .digi-form h3 > a {
        color: #ffffff;
        text-decoration: underline;
    }
</style>


<!-- Link generator form start-->
<section class="payment">
    <img src="<?=base_url()?>assets/images/aston-technologies-logo.svg" style="margin: 40px auto 0;display: table;" width="450" height="150">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-md-offset-2 col-xs-12 payment-way">
            <div class="digi-form">
                <h3 class="m-1o">Link Generator for Payment</h3>
                <div class="row">
                    <!-- Form start -->
                    <form action="<?=base_url()?>Payment_Terminal/process" method="post" id="linkForm" novalidate="novalidate">
                        <!-- Amount -->
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>First Name</label>
                                <input type="text" required name="first_name" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Last Name</label>
                                <input type="text" required name="last_name" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Email</label>
                                <input type="text" required name="email" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Number</label>
                                <input type="number" required name="number" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-12 website">
                            <hr>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Amount</label>
                                <input type="number" min="1" name="amount" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Tax %</label>
                                <input type="number"  name="tax" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Discount</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="number"  name="discount" placeholder="" autocomplete="off" step="any">
                                    </div>
                                    <div class="col-md-6">
                                        <select name="discount_type">
                                            <option>Select Option</option>
                                            <option value="Percent">In Percent</option>
                                            <option value="InAmount">In Amount</option>
                                        </select>
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>
                        <!-- Currency -->
                        <div class="col-md-6">
                            <div class="country-select">
                                <label>Currency</label>
                                <select name="currency" id="changeCurrency">
                                    <option selected="" value="USD">USD</option>
                                    <option value="CAD">CAD</option>
                                </select>
                            </div>
                        </div>
                        
                        <!-- Description -->
                        <div class="col-md-12">
                            <label>Description</label>
                            <textarea cols="60" rows="3" placeholder="" name="item"></textarea>
                        </div>

                        <div class="col-md-12">
                            <hr>
                        </div>
                        <!-- Website start -->
                        <div class="col-md-12 website">
                            <h4> Select Brand</h4>
                        </div>
                        <?php 
                        $count = count($brands);
                        foreach($brands as $brand){?>
                        <div class="col-md-4">
                            <div class="checkout-form-list">
                                <div class="form-group radio-green">
                                    <input id="<?=$brand["BrandID"]?>" name="site" value="<?=$brand["BrandID"]?>" type="radio" <?php if($count==$brand["BrandID"]){echo 'checked=""';}?>>
                                    <label for="<?=$brand["BrandID"]?>"><?=$brand["BrandName"]?></label>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <!-- Website end -->
                        <div class="clearfix"></div>
                        <!-- Package type start -->
                        <div class="package-box">
                            <div class="col-md-12 ">
                                <div class="col-md-12" id="features-div">
                                    <?php 
                                    foreach($services as $service){
                                    ?>
                                    <div class="col-md-6">
                                        <label class="package-error">
                                            <input type="checkbox" name="package[]" value="<?=$service["ServiceName"]?> ">
                                            <?=$service["ServiceName"]?> 
                                        </label>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Package type end -->
                        <div class="clearfix"></div>
                        <div class="clearfix"></div>            
                        <!-- Payment Type -->
                        <div class="col-md-6" id="payment_type-div">
                            <label>No. Of Transactions</label>
                            <!--<select name="payment_type">-->
                            <select name="pt" id="pt">
                                <!--<option value="1"  selected="">Single</option>-->
                                <option value="1" selected="">One Transaction</option>
                                <option value="3">Three Transactions</option>
                                <option value="4">Four Transactions</option>
                                <!--<option value="2">Double</option>-->
                                <!--<option value="1">Single</option>-->
                            </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 website">
                            <h4> Select Agent</h4>
                        </div>
                        <div class="col-md-6">
                            <select name="agent" class="form-control">
                                <option>Select Agent</option>
                                <?php 
                                
                                foreach($users as $key =>$user){
                                if($user["fk_parent_id"]==18 || $user["fk_parent_id"]==27 || $user["fk_parent_id"]==28 || $user["fk_parent_id"]==29  || $user["fk_parent_id"]==41 || $user["fk_parent_id"]==45  || $user["fk_parent_id"]==44){}
                                else{
                                    ?>
                                    <option value="<?=$user["fk_parent_id"]?>"><?=$user["first_name"].' '.$user["last_name"];?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <!-- DU Email  -->
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <input type="submit" value="submit" class="btn">
                        </div>
                    </form>
                    <!-- Form end -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Link generator form end-->





<!-- Js start -->

<script type="text/javascript" src="<?=base_url()?>assets/front/js/jquery.1.12.4.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/jquery.validate.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/form.validation.js"></script>

<!-- Js end -->





<script type="text/javascript">

    $("body").on('click','.payment_type_checkbox',function(){

        var id = $(this).val();

        //if(id <= 2)
        if((id == 2) || (id==6))

        {

            //$("#payment_type-div").show('slow');

        }

        else

        {

            //$("#payment_type-div").hide('slow');

        }



    });

    // On change to show customer email start
    $('select[name=payment_type]').on('change',function(){
        var val = $(this).val();
        if(val=='2'){
            $('#custom_email').show('slow');
            $('input[name=custom_email]').prop('required',true);
        }
        else{
            $('#custom_email').hide('hide');
            $('input[name=custom_email]').prop('required',false);
        }
    });
    // On change to show customer email end

    // Show form option start
    // 3 = Octachat.com
    // 4 = DesignQuotations.com
    // 5 = AppOcta.com
    // 6 = DesignParamount.com
    // 8 = Perfecent.com
    // 18 = Cmolds.com
    // 20 = Digitonics.com

    // Excluding DU list end

    // 26 = SEO
    // 27 = finest content writing
    // 29 = ghost book writing

    
    // Show FCW form option end

</script>





<script>
    $(document).ready(function(){
       $("#changeCurrency").change(function(){
           
          var dropDownValue = $(this).val();
          $("#pt option:nth-child(1)").remove();
          if(dropDownValue == 'GBP' || dropDownValue == 'CAD'){
              $('<option/>')
              .val(1)
              .text('Single')
              .appendTo('#pt')
              
          }
          else{
              $('<option/>')
              .val(3)
              .text('Triple')
              .appendTo('#pt')
          }
       });
    });
</script>


</body></html>