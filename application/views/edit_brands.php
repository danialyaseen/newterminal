<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>New Payment Form</title>

    <!-- Css start -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/front/css/style.css">
    <!-- Css end -->

</head>
<body>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    .digi-form h3 > a {
        color: #ffffff;
        text-decoration: underline;
    }
</style>


<!-- Link generator form start-->
<section class="payment">
    <img src="<?=base_url()?>assets/images/aston-technologies-logo.svg" style="margin: 40px auto 0;display: table;" width="450" height="150">
    <div class="container">
        <div class="col-lg-8 col-md-8 col-sm-12 col-md-offset-2 col-xs-12 payment-way">
            <div class="digi-form">
                <h3 class="m-1o">Create Brands for Payment</h3>
                <div class="row">
                    <!-- Form start -->
                    <form action="<?=base_url()?>brands/edit_process"  enctype="multipart/form-data" method="post" id="linkForm" novalidate="novalidate">
                        <!-- Amount -->
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Brand Name (Enter Link with out HTTP/HTTPS)</label>
                                <input required type="text" name="BrandName" placeholder="" value="<?=$brands[0]['BrandName']?>" autocomplete="off" step="any">
                                <input required type="hidden" name="BrandID" placeholder="" value="<?=$brands[0]['BrandID']?>" autocomplete="off" step="any">
                            </div>
                        </div>
                        <!-- Amount -->
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Brand Biling Email</label>
                                <input required type="email" name="BrandBillingEmail" value="<?=$brands[0]['BrandBillingEmail']?>" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div>
                        <!-- Amount -->
                        
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Brand Descriptor</label>
                                <input required type="text" name="BrandDescriptor" placeholder="" value="<?=$brands[0]['BrandDescriptor']?>" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Terms & Condition Link</label>
                                <input required type="text" name="terms" placeholder="" value="<?=$brands[0]['BrandTerms']?>" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Privacy & Policy Link</label>
                                <input required type="text" name="privacy" placeholder="" value="<?=$brands[0]['BrandPolicy']?>" autocomplete="off" step="any">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="country-select">
                                <label>Stripe</label>
                                <select required name="BrandStripe" id="changeCurrency">
                                    <option value='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|pk_live_51J6jkPGGVC7U79BoC6P71mHDFaIimHBPRZt5rtJVsQJc4xYU4dE7vE6l3NAY2C9hlhFs3lUIXMwibVMpPUkRoQ5x00OqIGH6qi'>The Webnificient Stripe</option>
                                    <option value='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|pk_live_51Kdad4LST7P0FGkCaUvEdaVxHPcucSLyNL02cDDtpR03TxPMyyivOXHjKAqyvYheJ6CMK0ph29VtiOKgEUwEWp6200VhpICRfq'>Design Aestro Stripe</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkout-form-list">
                                <label>Brand Logo</label>
                                <input required type="file" name="BrandLogo" placeholder="" autocomplete="off" step="any">
                            </div>
                        </div> 
                        <div class="col-md-12">
                            
                            <input type="submit" value="submit" class="btn">
                        </div>                       
                    </form>
                    <!-- Form end -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Link generator form end-->





<!-- Js start -->

<script type="text/javascript" src="<?=base_url()?>assets/front/js/jquery.1.12.4.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/jquery.validate.js"></script>

<script type="text/javascript" src="<?=base_url()?>assets/front/js/form.validation.js"></script>

<!-- Js end -->





<script type="text/javascript">

    $("body").on('click','.payment_type_checkbox',function(){

        var id = $(this).val();

        //if(id <= 2)
        if((id == 2) || (id==6))

        {

            $("#payment_type-div").show('slow');

        }

        else

        {

            $("#payment_type-div").hide('slow');

        }



    });

    // On change to show customer email start
    $('select[name=payment_type]').on('change',function(){
        var val = $(this).val();
        if(val=='2'){
            $('#custom_email').show('slow');
            $('input[name=custom_email]').prop('required',true);
        }
        else{
            $('#custom_email').hide('hide');
            $('input[name=custom_email]').prop('required',false);
        }
    });
    // On change to show customer email end

    // Show form option start
    // 3 = Octachat.com
    // 4 = DesignQuotations.com
    // 5 = AppOcta.com
    // 6 = DesignParamount.com
    // 8 = Perfecent.com
    // 18 = Cmolds.com
    // 20 = Digitonics.com

    // Excluding DU list end

    // 26 = SEO
    // 27 = finest content writing
    // 29 = ghost book writing

    $('input[name=site]').change(function(){
        //
        //var brands = ['5','20','26','27','29'];
        //var brands = ['3','4','5','6','8','18','20','26','27','29'];
        //var brands = ['3','4','5','6','8','18','20','26'];  // Remove SEOProhub
        //var brands = ['3','4','5','6','8','18','20','43','111'];  
        //var brands = ['3','4','5','6','8','18','20','111'];  // Remove 43 brand
        var brands = ['3','4','5','6','8','18','20'];  // Remove 43 brand
        var brand_id = $(this).val();
        if($.inArray(brand_id, brands) !== -1){
            // Unchecked all features checkbox
            $('#features-div input').prop('checked', false);
            $('#features-div').hide('slow');
            //$('#others-div').show('slow');
            // check other feature checkbox
            $('#others-div input').prop('checked', true);
            // Show du email
            $('#du-box').hide('slow');
        }
        else{
            $('#others-div input').prop('checked', false);
            //$('#others-div').hide('slow');
            $('#features-div').show('slow');
            // Hide du email
            $('#du-box').show('slow');
        }
    });
    // Show FCW form option end

</script>





<script>
    $(document).ready(function(){
       $("#changeCurrency").change(function(){
           
          var dropDownValue = $(this).val();
          $("#pt option:nth-child(1)").remove();
          if(dropDownValue == 'GBP' || dropDownValue == 'CAD'){
              $('<option/>')
              .val(1)
              .text('Single')
              .appendTo('#pt')
              
          }
          else{
              $('<option/>')
              .val(3)
              .text('Triple')
              .appendTo('#pt')
          }
       });
    });
</script>


</body></html>