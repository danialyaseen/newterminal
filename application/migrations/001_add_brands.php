<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_brands extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'BrandID' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'BrandName' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'BrandBillingEmail' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'BrandLogo' => array(
                            'type' => 'VARCHAR',
                            'constraint' => '255',
                        ),
                        'BrandDescriptor' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'BrandStripe' => array(
                                'type' => 'longtext',
                        ),
                        'BrandCreateDate' => array(
                            'type' => 'timestamp',
                            'null'=>true,
                        ),
                ));
                $this->dbforge->add_key('BrandID', TRUE);
                $this->dbforge->create_table('Brands');
        }

        public function down()
        {
                $this->dbforge->drop_table('Brands');
        }
}