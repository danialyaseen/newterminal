<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_services extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'ServiceID' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'ServiceName' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'ServiceCreateDate' => array(
                            'type' => 'timestamp',
                            'null'=>true,
                        ),
                ));
                $this->dbforge->add_key('ServiceID', TRUE);
                $this->dbforge->create_table('services');
        }

        public function down()
        {
                $this->dbforge->drop_table('services');
        }
}