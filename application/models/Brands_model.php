<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Brands_model extends CI_Model {
    public function AddBrands($data){
        $this->db->insert('Brands',$data);
        return true;
    }
    public function EditBrand($array,$BrandID){
        $this->db->where('BrandID', $BrandID);
        $result = $this->db->update('brands', $array);
        return true;
    }
    public function DeleteBrand(){}
    public function ShowBrand(){
        $this->db->select('*');
        $this->db->from("Brands");
        $this->db->order_by("BrandID","DESC");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function ShowBrandWithSearch($data){
        $this->db->select('*');
        $this->db->from("Brands");
        $this->db->where($data);
        $this->db->order_by("BrandID","DESC");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function ShowCustomerSearch($data){
        $this->db->select('*');
        $this->db->from("generate_link");
        $this->db->where($data);
        $this->db->order_by("LinkID","DESC");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function AddMarketingPreference(){}
    public function EditMarketingPreference(){}
    public function DeleteMarketingPreference(){}
    public function ShowMarketingPreference(){}
}

?>