<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Services_model extends CI_Model {
    public function AddServices($data){
        $this->db->insert('services',$data);
        return true;
    }
    public function EditServices($array,$ServiceID){
        $this->db->where('ServiceID', $ServiceID);
        $result = $this->db->update('services', $array);
        return true;
    }
    public function DeleteBrand(){}
    public function ShowServices(){
        $this->db->select('*');
        $this->db->from("services");
        $query = $this->db->get();
        return $query->result_array();
    }
    public function ShowServiceswithsearch($data){
        $this->db->select('*');
        $this->db->from("services");
        $this->db->where($data);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function AddMarketingPreference(){}
    public function EditMarketingPreference(){}
    public function DeleteMarketingPreference(){}
    public function ShowMarketingPreference(){}
}

?>