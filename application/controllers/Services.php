<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
        $msg = $this->uri->segment(2);
        if(isset($msg) || $msg!=''){
            $data["msg"] = $msg;
        }
        $data["services"] = $this->servicesmodel->ShowServices();
		$this->load->view('services',$data);
	}
    public function add(){
        
        $data["brands"] = $this->brandsmodel->ShowBrand();
		$this->load->view('add_services');
    }
    public function process(){
        $array = array(
            "ServiceName"=>$this->input->post("ServiceName"),
            "ServiceCreateDate"=>date('Y-m-d H:i:s')
        );
        $data["save"] = $this->servicesmodel->AddServices($array);
        if($data["save"]==1){
            redirect('services/?message=success', 'refresh');
        }
        else{
            echo "Data Failed";
        }
    }
    public function edit($slug=null){
        $array = array("ServiceID"=>$slug);
        $data["services"] = $this->servicesmodel->ShowServiceswithsearch($array);
        $this->load->view('edit_services',$data);
    }
    public function edit_process(){
        $ServiceID = $_POST["ServiceID"];
        $array = array(
                "ServiceName"=>$this->input->post("ServiceName"),
            );
        $data["save"] = $this->servicesmodel->EditServices($array,$ServiceID);
        if($data["save"]==1){
            redirect('services/?message=success', 'refresh');
        }
        else{
            echo "Data Failed";
        }
    }
}
