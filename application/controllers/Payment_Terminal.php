<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_Terminal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function create_link($key){
        if($key=='Aston1234'){
            $data["services"] = $this->servicesmodel->ShowServices();
            $data["brands"] = $this->brandsmodel->ShowBrand();
            $curl = curl_init('https://crm.projectheadways.com/restapis/index.php?type=users&start_date=2022-11-01&end_date=2022-11-15&team=D');
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            $response = curl_exec($curl);
            $result_1 = json_decode($response,true);
            $data["users"] = $result_1;
            $this->load->view('create_link',$data);
        }
        else{
            redirect('404', 'refresh');
        }
        
    }
    public function process(){
        $package = '';
        foreach($_POST["package"] as $packages){
            $package .= $packages.",";
        }
        $package = rtrim($package, ',');

        $array = array(
                "first_name"            =>$_POST["first_name"],
                "last_name"             =>$_POST["last_name"],
                "email"                 =>$_POST["email"],
                "number"                =>$_POST["number"],
                "amount"                =>$_POST["amount"],
                "tax"                   =>$_POST["tax"],
                "currency"              =>$_POST["currency"],
                "description"           =>$_POST["item"],
                "brand"                 =>$_POST["site"],
                "agentID"               =>$_POST["agent"],
                "services"              =>$package,
                "payment_type"          =>$_POST["pt"],
                "discount"              =>$_POST["discount"],
                "discount_type"         =>$_POST["discount_type"],
        );
        $this->db->insert("generate_link",$array);
        $insertID = $this->db->insert_id();
        redirect('paynow/'.$insertID, 'refresh');
        //echo $package;
        //print_r($_POST);
    }
    public function paynow($slug=null){
        /*require_once('application/libraries/stripe-php/init.php');
    
        $stripe = new \Stripe\StripeClient(
            $this->config->item('stripe_secret')
          );
        $bt = $stripe->charges->all(
            []
          );
        echo '<pre>';
        print_r($bt);
        echo '</pre>';*/
        $array = array("LinkID"=>$slug);
        $data["customerdata"] = $this->brandsmodel->ShowCustomerSearch($array);
        
        //
        if(empty($data["customerdata"])){

            $this->load->view('invalidlink',$data);
        }
        else if($data["customerdata"][0]["status"]==1){
            $arry = array("BrandID"=>$data["customerdata"][0]["brand"]);
        $data["ShowBrandWithSearch"] = $this->brandsmodel->ShowBrandWithSearch($arry);
            $this->load->view('paidlink',$data);
        }
        else{
            $site = $data["customerdata"][0]["brand"];
            $array = array("BrandID"=>$site);
            $data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);

            $this->load->view('paynow',$data);
        }
        //$site = $_SESSION["packagedetails"]["site"];
        //$array = array("BrandID"=>$site);
        //$data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);
        //$this->load->view('paynow',$data);
    }
    public function api(){
        $curl = curl_init('https://crm.projectheadways.com/restapis/index.php?type=users&start_date=2022-11-01&end_date=2022-11-15&team=D');
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($curl);
        $result_1 = json_decode($response,true);
        print_r($result_1);
        foreach($result_1 as $key=>$result){
            echo $result["first_name"];
        }
        
    }
    public function handleStripePayment(){
        $linkID = $_POST["linkID"];
        $array = array("LinkID"=>$linkID);
        $data["customerdata"] = $this->brandsmodel->ShowCustomerSearch($array);
        $site = $data["customerdata"][0]["brand"];
        $array = array("BrandID"=>$site);
        $data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);
        $fname = $_POST["fname"];
        $lname = $_POST["lname"];
        $company = $fname.' '.$lname;
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $statementdescriptor =  $data["brands"][0]["BrandDescriptor"];
        

        $fname = $_POST["fname"];
        $lname = $_POST["lname"];
        $customer_email = $_POST["email"];
        $phone = $_POST["phone"];
        $site = $data["customerdata"][0]["brand"];
        $amount = $_POST["amount"];
        $currency = $data["customerdata"][0]["currency"];
        $pkg   = $data["customerdata"][0]["description"];
        $agent =$data["customerdata"][0]["agentID"];
        $item = $data["customerdata"][0]["services"];
        $item = str_replace(" ","-",$item);
        $success = '';
        require_once('application/libraries/stripe-php/init.php');
        \Stripe\Stripe::setApiKey($data["brands"][0]["BrandStripe"]);
        $customer = \Stripe\Customer::create([
            'metadata'=>array(
                'firstName' =>$fname,
                'lastName' => $lname,
                'company' => $company,
                'email' => $email,
                'phone' => $phone,
                /*// Billing
                'billingAddress' => [
                    'firstName' => isset($_POST['fname']) ? $_POST['fname'] : $firstName,
                    'lastName' => isset($_POST['lname']) ? $_POST['lname'] : $firstName,
                    'company' => $company,
                    'streetAddress' => $address,
                    'locality' => $city,
                    'region' => $state,
                    'postalCode' => $zip
                ]*/
                ),
                "source" => $this->input->post('stripeToken'),
            'email'=>$email,
            'description' => $data["customerdata"][0]["description"],
          ]);
        //\Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));
        
        if($data["customerdata"][0]["payment_type"]==3){
            $customer_id = $customer->id;
            $rand = floatVal('2.'.rand(20, 90));
            $rand2 = floatVal('2.'.rand(20, 90));
            $amount = $_POST["amount"];
            $amount = $amount - $rand;
            $amount = $amount - $rand2;
            $tr1 = \Stripe\Charge::create ([
                "amount" => 100 * $rand,
                "currency" => $data["customerdata"][0]["currency"] ,
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            $tr2 = \Stripe\Charge::create ([
                "amount" => 100 * $rand2,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            $tr3 = \Stripe\Charge::create ([
                "amount" => 100 * $amount,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"] 
            ]);
            if($tr1!='' && $tr2!='' && $tr3!=''){
                $success = 1;
            }
        }
        if($data["customerdata"][0]["payment_type"]==1){
            $customer_id = $customer->id;
            $amount = $_POST["amount"];
            $tr1 = \Stripe\Charge::create ([
                "amount" => 100 * $amount,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            if($tr1!=''){
                $success = 1;
            }
        }
        else{
            $customer_id = $customer->id;
            $rand = floatVal('2.'.rand(20, 90));
            $rand2 = floatVal('2.'.rand(20, 90));
            $rand3 = floatVal('2.'.rand(20, 90));
            $amount = $_POST["amount"];
            $amount = $amount - $rand;
            $amount = $amount - $rand2;
            $amount = $amount - $rand3;
            $tr1 = \Stripe\Charge::create ([
                "amount" => 100 * $rand,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            $tr2 = \Stripe\Charge::create ([
                "amount" => 100 * $rand2,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            $tr3 = \Stripe\Charge::create ([
                "amount" => 100 * $rand3,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            $tr4 = \Stripe\Charge::create ([
                "amount" => 100 * $amount,
                "currency" => $data["customerdata"][0]["currency"],
                "customer" => $customer_id,
                "statement_descriptor" => $statementdescriptor,
                "description" => $data["customerdata"][0]["description"]
            ]);
            if($tr1!='' && $tr2!='' && $tr3!='' && $tr4!=''){
                $success = 1;
            }
        }

        
        if($success = 1){
            $this->session->set_flashdata('success', 'Payment has been successful.');
            
            
            $url = 'https://crm.projectheadways.com/restapis/index.php?currency='.$currency.'&type=create_customer&fname='.$fname.'&lname='.$lname.'&customer_email='.$customer_email.'&phone='.$phone.'&site='.$site.'&pkg='.$pkg.'&agent='.$agent.'&item='.$item.'&amount='.$amount;
            $client = curl_init($url);
            curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
            $response = curl_exec($client);
            $result_1 = json_decode($response);
            $datas = array("status"=>1);
            $this->db->where('LinkID', $linkID);
            $this->db->update('generate_link', $datas);
            $array = array("BrandID"=>$site);
            $data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);
            redirect('success/'.$linkID, 'refresh');
            //$this->load->view('success',$data);
        }
        
        //redirect('/make-stripe-payment', 'refresh');
        //print_r($_SESSION);
        
    }
    public function success($slug=null){
        $linkID = $slug;
        $array = array("LinkID"=>$linkID);
        $data["customerdata"] = $this->brandsmodel->ShowCustomerSearch($array);
        $site = $data["customerdata"][0]["brand"];
        $array = array("BrandID"=>$site);
        $data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);
        $this->load->view('success',$data);
    }
}
