<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index()
	{
        $msg = $this->uri->segment(2);
        if(isset($msg) || $msg!=''){
            $data["msg"] = $msg;
        }
        $data["brands"] = $this->brandsmodel->ShowBrand();
		$this->load->view('brands',$data);
	}
    public function add(){
        
        $data["brands"] = $this->brandsmodel->ShowBrand();
		$this->load->view('add_brands');
    }
    public function process(){
    	$result = $_POST['BrandStripe'];
    	$result_explode = explode('|', $result);
        $name =  $_FILES["BrandLogo"]["name"];
		
        $newname = $name;
        $path = "assets/images/brands";
		$fileExt = pathinfo($_FILES["BrandLogo"]["name"], PATHINFO_EXTENSION);
		//print_r($_FILES);
		$image_new=fileuploadCI('BrandLogo',$path,$newname);
        if($image_new==1){
            $array = array(
				"BrandName"=>$this->input->post("BrandName"),
                "BrandBillingEmail"=>$this->input->post("BrandBillingEmail"),
				"BrandLogo"=>$path.'/'.$newname,
                "BrandDescriptor"=>$this->input->post("BrandDescriptor"),
                "BrandStripe"=>$result_explode[0],
                "BrandPUBKEY"=>$result_explode[1],
                "BrandTerms"=>$this->input->post("terms"),
                "BrandPolicy"=>$this->input->post("privacy"),
				"BrandCreateDate"=>date('Y-m-d H:i:s')
			);
			$data["save"] = $this->brandsmodel->AddBrands($array);
			if($data["save"]==1){
				redirect('brands/?message=success', 'refresh');
			}
			else{
				echo "Data Failed";
			}
        }
    }
    public function edit($slug=null){
    	$array = array("BrandID"=>$slug);
    	$data["brands"] = $this->brandsmodel->ShowBrandWithSearch($array);
    	$this->load->view('edit_brands',$data);
    }
    public function edit_process(){
    	$BrandID = $_POST['BrandID'];
    	$result = $_POST['BrandStripe'];
    	$result_explode = explode('|', $result);
        $name =  $_FILES["BrandLogo"]["name"];
		
        $newname = $name;
        $path = "assets/images/brands";
		$fileExt = pathinfo($_FILES["BrandLogo"]["name"], PATHINFO_EXTENSION);
		//print_r($_FILES);
		$image_new=fileuploadCI('BrandLogo',$path,$newname);
        if($image_new==1){
            $array = array(
				"BrandName"=>$this->input->post("BrandName"),
                "BrandBillingEmail"=>$this->input->post("BrandBillingEmail"),
				"BrandLogo"=>$path.'/'.$newname,
                "BrandDescriptor"=>$this->input->post("BrandDescriptor"),
                "BrandStripe"=>$result_explode[0],
                "BrandPUBKEY"=>$result_explode[1],
                "BrandTerms"=>$this->input->post("terms"),
                "BrandPolicy"=>$this->input->post("privacy"),
				"BrandCreateDate"=>date('Y-m-d H:i:s')
			);
			$data["save"] = $this->brandsmodel->EditBrand($array,$BrandID);
			if($data["save"]==1){
				redirect('brands/?message=success', 'refresh');
			}
			else{
				echo "Data Failed";
			}
        }
    }
}
